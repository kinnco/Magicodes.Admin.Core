﻿ABOUT LOCALIZATION FILES
------------------------------------------------------------
This folder contains localization files for the application.

You can add more languages here with appropriate postfixes.
For instance, to add a German localization, you can add "Admin-de.xml" or "Admin-de-DE.xml".

See http://www.aspnetboilerplate.com/Pages/Documents/Localization for more information.
