﻿namespace Magicodes.Admin.Core.Custom.LogInfos
{
    /// <summary>
    /// 支付渠道
    /// </summary>
    public enum PayChannels
    {
        /// <summary>
        /// 微信支付
        /// </summary>
        WeChatPay = 0,

        /// <summary>
        /// 支付宝支付
        /// </summary>
        AliPay = 1
    }
}