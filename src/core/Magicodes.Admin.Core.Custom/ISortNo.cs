﻿namespace Magicodes.Admin.Core.Custom
{
    /// <summary>
    /// 自定义排序接口
    /// </summary>
    public interface ISortNo
    {
        /// <summary>
        /// 排序号
        /// </summary>
        long? SortNo { get; set; }
    }
}
