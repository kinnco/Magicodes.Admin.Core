﻿namespace Magicodes.Admin.Core.Custom.Authorization
{
    /// <summary>
    /// OpenId来自
    /// </summary>
    public enum OpenIdPlatforms
    {
        /// <summary>
        /// 微信公众号
        /// </summary>
        WeChat = 0,

        /// <summary>
        /// 微信小程序
        /// </summary>
        WechatMiniProgram = 1
    }
}