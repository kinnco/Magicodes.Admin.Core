# Magicodes.Admin

>加群 85318032 获取最新动态和文档。

## 说明

Magicodes.Admin，是一套高效率、易扩展、基础设施强大、代码生成完备、理念和技术先进的敏捷开发框架，同时也是一套分布式、跨平台、多终端（包括Android、IOS、H5、小程序）支持的统一开发框架和解决方案。

其在ABP和ASP.NET Zero的基础上进行了封装和完善，并且编写了相关的工具（代码生成）、组件（云存储、支付、微信等等）、生成模板。

目前框架中包含以下开发组件、套件、解决方案、理念：

* 通用权限
* 多租户
* 版本
* 组织机构
* 多语言
* 审计日志（操作审计和数据审计）
* 缓存
* 日志
* 设置管理器
* **短信服务（Magicodes.Sms）**
* **支付（Magicodes.Pay，微信、支付宝）**
* **微信SDK（Magicodes.WeChat.SDK）**
* **小程序SDK（Magicodes.WeChat.SDK）**
* 通用异常处理
* **领域驱动**
* **依赖注入**
* **接口权限以及授权**
* **在线接口文档（Magicodes.SwaggerUI+Swagger UI）**
* 数据验证
* 调度任务（Quartz）
* 后台任务（Hangfire）
* 数据筛选器（租户筛选器、软删除、是否激活）
* **跨平台（目前基于.NET Core 2.1）**
* 通知系统
* 即时消息（SignalR）
* ORM和数据迁移(Entity Framework Core)
* **通用导入导出（Magicodes.ExporterAndImporter）**
* **通用存储（Magicodes.Storage，支持本地存储和阿里云存储）**
* 全国行政区域抓取和初始化（Magicodes.Districts）
* **移动端统一开发解决方案和模板（Angular+Ionic）**
* 前后端分离
* **后台前端解决方案和UI（Angular、primeng、bootstrap）**
* **简单CMS**
* **移动端通用接口（登陆注册找回密码等）**
* 邮件服务
* **移动端多语言支持**
* **交易流水以及多国货币支持**
* **大量后台UI组件（除了常用组件，还支持Tree Table、图片展示、文件批量上传、枚举下拉、关联项下拉、审计）**
* 单元测试（后台服务、移动端服务）
* **代码生成（后台服务、后台UI功能、多语言定义、权限定义、移动端服务）**
* **一键部署（后台服务、前台服务、后台前端）**
* **接口调用代码生成（nswag，后台前端和移动端前端）**
* **升级工具**
* **配套项目/产品开发流程（<https://gitee.com/xl_wenqiang/xinlai_devprocess>）**

加群 **85318032** 获取最新动态和文档。

## 生成服务

 高级版附送代码生成工具以及相关源代码。
![生成结构图](./res/代码生成.png)

## Demo

<https://demoadmin.xin-lai.com>

* 账号：Admin
* 密码：123456abcD

>注意：演示环境不一定是最新代码_

## 授权文档下载：

1. [Magicodes.Admin源码基础版授权合同](Magicodes.Admin源码基础版授权合同.doc)
2. [Magicodes.Admin源码高级版授权合同](Magicodes.Admin源码高级版授权合同.doc)

## 推荐开发环境

![推荐开发环境](./documents/Magicodes.Admin推荐开发环境.png)

已升级到到.NET Core 2.1，请先下载SDK：<https://www.microsoft.com/net/download/windows>

## 开发文档

[开发文档](documents/教程/0.起步.md)

## 官方博客

<http://www.cnblogs.com/codelove/>

## 官方网址

<http://xin-lai.com>

## 其他开源库地址

<https://github.com/xin-lai>

## 相关QQ群

* 85318032（编程交流群1）

## 小店地址

<https://shop113059108.taobao.com/>